function [ expected ] = test( )
%TEST Summary of this function goes here
%   Detailed explanation goes here
[feature_vector,block_size] = read_training_file();


img = imread('testimg.bmp');
img = image_inverse(img);
img = image_trim(img);
img = image_adjust(img,block_size);

%imshow(img)

test_file_feature_vector = get_feature_vector(img , block_size);

min = 9999999999999999;
expected = -1;

for i = 1:length(feature_vector)
    handler = 0;
    for j = 1:length(feature_vector{i})
        for k = 1:length(feature_vector{i}{j})
            for l = 1:length(feature_vector{i}{j}{k})
                handler = handler + (feature_vector{i}{j}{k}{l} - test_file_feature_vector{j}{k}{l})^2;
            end
        end
    end
    
    handler = sqrt(handler);
    if handler < min
        min = handler;
        expected = i;
    end
end

expected = floor((expected-1) / 10);
end

