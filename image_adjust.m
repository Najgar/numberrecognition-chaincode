function [ A ] = image_adjust( A , block_size )
%UNTITLED6 Summary of this function goes here
%   Detailed explanation goes here
[row,col] = size(A);
additional_rows = block_size - rem(row,block_size);
if additional_rows > 0
    new_image = [A;repmat(zeros(1,col),additional_rows,1)];
    A = new_image;
end

[row,col] = size(A);
additional_cols = block_size - rem(col,block_size);
if additional_cols > 0
    new_image = [A(:,1:col) repmat(zeros(row,1),1,additional_cols)];
    A = new_image;
end
end

