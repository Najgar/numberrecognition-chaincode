function [ cx,cy ] = get_centroid( img )
%GET_CENTROID Summary of this function goes here
%   Detailed explanation goes here
    [r,c] = find(img);
    
    cx = sum(r);
    cy = sum(c);
    
    if length(r) == 0
        cx = 0.0;
    else
        cx = cx/length(r);
    end

    if length(c) == 0
        cy = 0.0;
    else
        cy = cy/length(c);
    end

end

