function [A] = image_read(block_size)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

%img = imread('training/0_1.bmp')
%subplot(1,1,1)
%imshow(img)

list = dir('training');
files_size = length(list);
A = []
for i= 3:files_size
    name = list(i).name;
    img = imread(strcat('training/',list(i).name));
    img = image_inverse(img);
    img = image_trim(img);
    img = image_adjust(img,block_size);
    
    value = {struct('name',name);
            struct('img',img)
            };
    A{i-2} = value;
end
end

