function [ feature_vector ] = get_feature_vector( A , block_size )
%GET_FEATURE_VECTOR Summary of this function goes here
%   Detailed explanation goes here
    feature_vector = {};
    partition = {};
    for i = 1:block_size
        feature_vector{i} = {};
        partition{i} = {};
        for j = 1:block_size
            feature_vector{i}{j} = {};
            partition{i}{j} = 0;
            for k = 1:8
                feature_vector{i}{j}{k} = 0;
            end
        end
    end

    [cx,cy] = get_centroid(A);

    x = -1 ; 
    y = -1 ; 
    [r c]=size(A);
    for i = 1 : r 
        for j = 1 : c 
            if (A (i , j) == 1 )
                x = i ; 
                y = j ;
                break ;
            end 
        end 
        if x == i && y == j
            break ; 
        end      
    end
    
    b=bwtraceboundary(A,[x y],'N');
    R = get_radius(b,cx,cy);
    
    for i=1:length(b)
        x1 = b(i,1);
        y1 = b(i,2);
        if (i+1 > r)
            break;
        end
        x2 = b(i+1,1);
        y2 = b(i+1,2);
    
        y= y2-y1;
        x= x2-x1;
        
        temp = (cx - x1)^2;
        temp2 = (cy - y1)^2;
        res = sqrt(temp + temp2);
        t = floor((res / R) * block_size) + 1;
        if (t > block_size)
            t = block_size;
        end
        ceta = atand(y1-cy/x1-cx);
        if (ceta < 0)
            ceta = 360 + ceta;
        end
        s = floor((ceta / 360 ) * block_size) + 1;
        if (s > block_size)
            s = block_size;
        end
        
        partition{t}{s} = partition{t}{s} + 1;
        if (y>=0 && x==0) 
            feature_vector{t}{s}{1} = feature_vector{t}{s}{1}+1;
        elseif (y>0 && x>0) 
            feature_vector{t}{s}{2} = feature_vector{t}{s}{2}+1;
        
        elseif( y==0 && x>=0) 
            feature_vector{t}{s}{3} = feature_vector{t}{s}{3}+1;
        
        elseif (y<0 && x>0) 
            feature_vector{t}{s}{4} = feature_vector{t}{s}{4}+1;
    
        elseif (y<0 && x==0)  
            feature_vector{t}{s}{5} = feature_vector{t}{s}{5}+1; 
        
        elseif (y<0 && x<0)  
            feature_vector{t}{s}{6} = feature_vector{t}{s}{6}+1; 
      
        elseif (y==0 && x<=0) 
            feature_vector{t}{s}{7} = feature_vector{t}{s}{7}+1;
   
        elseif (x<0 && y>0) 
            feature_vector{t}{s}{8} = feature_vector{t}{s}{8}+1;
        end 
    end
    
    for i = 1:block_size
        for j = 1:block_size
            for k = 1:8
                if (partition{i}{j} > 0)
                    feature_vector{i}{j}{k} = feature_vector{i}{j}{k} / partition{i}{j};
                end
            end
        end
    end
end

