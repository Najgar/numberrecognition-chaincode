function [ feature_vector ] = train( )
%UNTITLED7 Summary of this function goes here
%   Detailed explanation goes here

prompt = 'How many block to train data with : ';
block_size = input(prompt);
A = image_read(block_size);
feature_vector = {};
fileID = fopen('training.txt','wt');
fprintf(fileID,'%d',block_size);
fprintf(fileID,'\n');
for i= 1:length(A)
    feature_vector{i} = get_feature_vector(A{i}{2}.img , block_size);
    for j = 1:length(feature_vector{i})
        for k = 1:length(feature_vector{i}{j})
            fprintf(fileID,'%d',feature_vector{i}{j}{k}{1});
            fprintf(fileID,'%s',',');
            fprintf(fileID,'%d',feature_vector{i}{j}{k}{2});
            fprintf(fileID,'%s',',');
            fprintf(fileID,'%d',feature_vector{i}{j}{k}{3});
            fprintf(fileID,'%s',',');
            fprintf(fileID,'%d',feature_vector{i}{j}{k}{4});
            fprintf(fileID,'%s',',');
            fprintf(fileID,'%d',feature_vector{i}{j}{k}{5});
            fprintf(fileID,'%s',',');
            fprintf(fileID,'%d',feature_vector{i}{j}{k}{6});
            fprintf(fileID,'%s',',');
            fprintf(fileID,'%d',feature_vector{i}{j}{k}{7});
            fprintf(fileID,'%s',',');
            fprintf(fileID,'%d',feature_vector{i}{j}{k}{8});
            fprintf(fileID,'%s\n',';');
        end
    end
    fprintf(fileID,'#');
    fprintf(fileID,'\n');
end
fclose(fileID);
end

